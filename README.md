# [note.harka.com](https://note.harka.com)

- Here are some information about the note editor.
- It has the very same functionalities on all devices and in all browsers, except for the keyboard shortcuts.

## SAVING

- Entered text is saved automatically after every letter to the browser's local storage.
- If the page is reloaded or closed and returned to, the text loads automatically.
- The text only gets deleted if the user deletes it or if the browser's history is cleared.

## UNDO

- Recently deleted or entered text can be undone by pressing Ctr/Cmd+Z.

## TEMPORARY TEXT FORMATTING

- Text can be formatted temporarily for a visual effect.
- Formatting options are bold (Ctr/Cmd+b), italic (Ctr/Cmd+i) and underline (Ctr/Cmd+u).
- Pasted text also keeps its original font style.
- This only works until the page is reloaded.
- It is because it's saved as plaintext, so the next time it's loaded it's without formatting.
- It can still be useful to add styles, for example when presenting.
